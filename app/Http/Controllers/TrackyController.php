<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\TrackingRequest;
use Illuminate\Http\Request;

class TrackyController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        return view('tracky.track');
	}

    public function trackPackage(TrackingRequest $request)
    {

        return view('tracky.show')->with($request->all());
    }
}
