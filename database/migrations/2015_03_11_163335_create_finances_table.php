<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('finances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('packageId')->unsigned();
            $table->integer('retailerId')->unsigned();
            $table->integer('depotId')->unsigned();
            $table->integer('providerId')->unsigned();
            $table->string('providerTrackingNumber', 255);
            $table->decimal('retailerCharge', 15, 2);
            $table->decimal('providerCharge', 15, 2);
            $table->timestamps();
        });

        Schema::table('finances', function ($table) {
            $table->foreign('packageId')->references('id')->on('packages');
            $table->foreign('retailerId')->references('id')->on('retailers');
            $table->foreign('depotId')->references('id')->on('depots');
            $table->foreign('providerId')->references('id')->on('providers');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('finances');
	}

}
