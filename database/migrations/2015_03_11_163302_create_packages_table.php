<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('senderName', 255);
            $table->string('senderEmail', 255);
            $table->string('senderPhone', 255);
            $table->string('shipmentFormPath', 255);
            $table->dateTime('dateOfCollection');
            $table->string('notes', 255);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('packages');
	}

}
