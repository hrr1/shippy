@extends('app')

@section('content')
    <h1 class="page-heading">Retailers</h1>
    @foreach($retailers as $retailer)
        <li>{{ $retailer }}</li>
    @endforeach
@endsection