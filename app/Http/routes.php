<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('ret', 'RetailerController');
Route::GET('tracky', 'TrackyController@index');
Route::GET('tracky/trackPackage', 'TrackyController@trackPackage');