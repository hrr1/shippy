@extends('app')

@section('content')
    <h1 class="page-heading">Find package</h1>

    @include('errors.list')
    {!! Form::open(['method' => 'GET', 'action' => 'TrackyController@trackPackage']) !!}

    <div class="form-group">
        {!! Form::label('tracking_number', 'Tracking number:') !!}
        {!! Form::text('tracking_number', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Track', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}

@endsection