<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class retailer extends Model {

	//
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

}
